<?
/*
Listado de las sesiones de calendario por cada
aula / grupo.
path: /backend/sesiones_aula_grupo.php
*/

$result=[];
try {
    include $_SERVER['DOCUMENT_ROOT'].'/backend/conexion.php';
} catch (Exception $e) {
    $result['error_info']=preg_replace('/[\x00-\x1F\x7F-\xFF]/', '',$e->getMessage());
    $result['error']=true;
    $result['error_l']=__FILE__." (".__LINE__.")";
    exit(json_encode($result,JSON_PARTIAL_OUTPUT_ON_ERROR));
}

//consulta
$sql = "
    SELECT *
    FROM cl_aulas_calendar
";
try {
    $query = $conexion->prepare($sql);
    $query->execute();
    $res = $query->fetchAll(PDO::FETCH_ASSOC);
} catch (Exception $e) {
    $result['error_info']=preg_replace('/[\x00-\x1F\x7F-\xFF]/', '',$e->getMessage());
    $result['error']=true;
    $result['error_l']=__FILE__." (".__LINE__.")";
    $result['sql']=$sql;
    exit(json_encode($result,JSON_PARTIAL_OUTPUT_ON_ERROR));
}
$res['sesiones_aula_grupo']=[];
foreach ($res as $key => $value) {
    if($value['aula_id']!=null){
        if(!isset( $result['sesiones_aula_grupo'][$value['aula_id']."-".$value['group_id']] )){
            $result['sesiones_aula_grupo'][$value['aula_id']."-".$value['group_id']] = [
                "aula_id"=>$value['aula_id']
                ,"group_id"=>$value['group_id']
                ,"sesiones"=>[]
            ];
        }
        $result['sesiones_aula_grupo'][$value['aula_id']."-".$value['group_id']]['sesiones'][$value['aula_calendar_id']]=$value;
    }
}


$conexion = null;
exit(json_encode($result,JSON_PARTIAL_OUTPUT_ON_ERROR));
?>
