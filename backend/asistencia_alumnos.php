<?
/*
listado de asistencias de alumnos por
aula / grupo.

path: /backend/asistencia_alumnos.php
*/

$result=[];
if(
    !isset($_GET['aula_id'])
    || !isset($_GET['grupo_id'])
){
    $result['error']=true;
    $result['error_l']=__FILE__." (".__LINE__.")";
    $result['error_info']='Minimun GET required.';
    exit(json_encode($result,JSON_PARTIAL_OUTPUT_ON_ERROR));
}

try {
    include $_SERVER['DOCUMENT_ROOT'].'/backend/conexion.php';
} catch (Exception $e) {
    $result['error_info']=preg_replace('/[\x00-\x1F\x7F-\xFF]/', '',$e->getMessage());
    $result['error']=true;
    $result['error_l']=__FILE__." (".__LINE__.")";
    exit(json_encode($result,JSON_PARTIAL_OUTPUT_ON_ERROR));
}
//consulta
$sql = "
    SELECT
        cl_students.*
        ,COUNT(cl_aulas_attendance.attendance_id) AS sessiones
        ,SUM(
            IF( cl_aulas_attendance.attendance_status IN (1,2) , 1, 0)
        ) AS presente
        ,SUM(
            IF( cl_aulas_attendance.attendance_status IN (1,2) , 0, 1)
        ) AS ausente
        ,SUM(
            IF( cl_aulas_attendance.attendance_status IN (1) , 1, 0)
        ) AS retraso
        ,SUM(
            IF( cl_aulas_attendance.attendance_status IN (4) , 1, 0)
        ) AS justificado
        ,SUM(
            (TIME_FORMAT(TIMEDIFF(
                cl_aulas_calendar.calendar_date_end
                , cl_aulas_calendar.calendar_date_ini),'%H')*60)
            + TIME_FORMAT(TIMEDIFF(
                cl_aulas_calendar.calendar_date_end
                , cl_aulas_calendar.calendar_date_ini),'%i')
        ) AS minutos
        ,SUM(
            IF( cl_aulas_attendance.attendance_status IN (1,2) ,
                (
                    (TIME_FORMAT(TIMEDIFF(
                        cl_aulas_calendar.calendar_date_end
                        , cl_aulas_calendar.calendar_date_ini),'%H')*60)
                    + TIME_FORMAT(TIMEDIFF(
                        cl_aulas_calendar.calendar_date_end
                        , cl_aulas_calendar.calendar_date_ini),'%i')
                ),0
            )
        ) AS minutos_asis
        ,SUM(
            IF( cl_aulas_attendance.attendance_status IN (4) ,
                (
                    (TIME_FORMAT(TIMEDIFF(
                        cl_aulas_calendar.calendar_date_end
                        , cl_aulas_calendar.calendar_date_ini),'%H')*60)
                    + TIME_FORMAT(TIMEDIFF(
                        cl_aulas_calendar.calendar_date_end
                        , cl_aulas_calendar.calendar_date_ini),'%i')
                ),0
            )
        ) AS minutos_jus


    FROM cl_students

    INNER JOIN cl_aulas_attendance
    ON cl_aulas_attendance.student_id = cl_students.student_id

    LEFT JOIN cl_aulas_calendar
    ON cl_aulas_attendance.attendance_date = cl_aulas_calendar.calendar_date_ini

    WHERE cl_aulas_attendance.group_id = ".$_GET['grupo_id']."
    AND cl_aulas_attendance.aula_id = ".$_GET['aula_id']."

    GROUP BY cl_students.student_id
";
try {
    $query = $conexion->prepare($sql);
    $query->execute();
    $res = $query->fetchAll(PDO::FETCH_ASSOC);
} catch (Exception $e) {
    $result['error_info']=preg_replace('/[\x00-\x1F\x7F-\xFF]/', '',$e->getMessage());
    $result['error']=true;
    $result['error_l']=__FILE__." (".__LINE__.")";
    $result['sql']=$sql;
    exit(json_encode($result,JSON_PARTIAL_OUTPUT_ON_ERROR));
}
$result['asistenca']=[];
foreach ($res as $key => $value) {
    $result['asistenca'][$value['student_id']]=$value;
}

$conexion = null;
exit(json_encode($result,JSON_PARTIAL_OUTPUT_ON_ERROR));
?>
