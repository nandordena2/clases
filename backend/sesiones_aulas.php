<?
/*
Listado de las sesiones de calendario por aula
path: /backend/sesiones_aulas.php
*/

$result=[];


try {
    include $_SERVER['DOCUMENT_ROOT'].'/backend/conexion.php';
} catch (Exception $e) {
    $result['error_info']=preg_replace('/[\x00-\x1F\x7F-\xFF]/', '',$e->getMessage());
    $result['error']=true;
    $result['error_l']=__FILE__." (".__LINE__.")";
    exit(json_encode($result,JSON_PARTIAL_OUTPUT_ON_ERROR));
}

//consulta
$sql = "
    SELECT *
    FROM cl_aulas_calendar
";
if(
    isset($_GET['date_ini'])
    && isset($_GET['date_end'])
){
    "
        WHERE DATE_FORMAT(cl_aulas_calendar.calendar_date_ini,'%Y-%m-%d')
            >= '".$_GET['date_ini']."'
        AND DATE_FORMAT(cl_aulas_calendar.calendar_date_end,'%Y-%m-%d')
            <= '".$_GET['date_end']."'
    ";
}
try {
    $query = $conexion->prepare($sql);
    $query->execute();
    $result['sesiones'] = $query->fetchAll(PDO::FETCH_ASSOC);
} catch (Exception $e) {
    $result['error_info']=preg_replace('/[\x00-\x1F\x7F-\xFF]/', '',$e->getMessage());
    $result['error']=true;
    $result['error_l']=__FILE__." (".__LINE__.")";
    $result['sql']=$sql;
    exit(json_encode($result,JSON_PARTIAL_OUTPUT_ON_ERROR));
}

$conexion = null;
exit(json_encode($result,JSON_PARTIAL_OUTPUT_ON_ERROR));
?>
