if(typeof fetchController != "function"){
    window.FetchController = class {constructor(){}};
    fetchController = new FetchController();
}
FetchController.prototype.sesiones_aula_grupo = ()=>{
    return fetch(
        "/backend/sesiones_aula_grupo.php"
    ).then(response=>{return response.json();});
};
FetchController.prototype.asistencia_alumnos = (aula_id,grupo_id)=>{
    if(
        typeof aula_id != "number"
        || typeof grupo_id != "number"
    ){
        return new Promise(function(resolve, reject) { reject(); });
    }

    return fetch(
        "/backend/asistencia_alumnos.php"
        +"?aula_id="+aula_id
        +"&grupo_id="+grupo_id
    ).then(response=>{return response.json();});
};
FetchController.prototype.sessiones_aulas = (date_ini,date_end)=>{
    let uri = "/backend/sesiones_aulas.php";
    if(
        date_ini != null
        && date_end != null
    ){
        let dateIni = new Date(date_ini).toLocaleString('en-CA',{hour12: false}).replace(/,.*/,'');
        let dateEnd = new Date(date_end).toLocaleString('en-CA',{hour12: false}).replace(/,.*/,'');
        if(
            typeof dateIni == "Invalid Date"
            || typeof dateEnd == "Invalid Date"
        ){
            return new Promise(function(resolve, reject) { reject(); });
        }
        uri+="?date_ini="+dateIni
        +"&date_end="+dateEnd
    }
    return fetch(
        uri
    ).then(response=>{return response.json();});
};
